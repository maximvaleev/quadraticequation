﻿using System.Runtime.Serialization;

namespace QuadraticEquation
{
    [Serializable]
    internal class NoRootsException : Exception
    {
        public NoRootsException(string? message) : base(message)
        {
        }
    }
}