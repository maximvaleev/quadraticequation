﻿using System.Runtime.Serialization;

namespace QuadraticEquation
{
    [Serializable]
    internal class InvalidUserInputException : Exception
    {
        public InvalidUserInputException(string? message) : base(message)
        {
        }
    }
}