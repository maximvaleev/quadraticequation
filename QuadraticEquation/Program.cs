﻿namespace QuadraticEquation;

internal class Program
{
    static void Main()
    {
        ConsoleHelpers helper = new();
        double[] coefficients;
        // запросить у пользователя коэффициенты
        do
        {
            try
            {
                Console.WriteLine("Поиск корней квадратного уравнения ax^2+bx+c=0");
                coefficients = helper.GetValuesInteractively("Введите коэффициенты a, b, c:", "a", "b", "c");
                break;
            }
            catch (InvalidUserInputException)
            {
                Console.Clear();
                int answer = ConsoleHelpers.AskUserForZeroOrOne("\nНеудачный ввод данных. Хотите повторить попытку?");
                if (answer == 1)
                    Console.Clear();
                else
                    return;
            }
        } 
        while (true);

        // рассчитать и вывести результат
        double x1, x2;
        try
        {
            (x1, x2) = SolveQuadraticEquation(coefficients[0], coefficients[1], coefficients[2]);

            if (!double.IsNaN(x2))
            {
                helper.FormatData("Корни уравнения",
                    Severity.Info,
                    new Dictionary<string, double>(2) { ["x1: "] = x1, ["x2: "] = x2 });
            }
            else
            {
                helper.FormatData("Корень уравнения",
                    Severity.Info,
                    new Dictionary<string, double>(1) { ["x: "] = x1 });
            }
        } 
        catch (NoRootsException exc) 
        {
            helper.FormatData(exc.Message, Severity.Warning);
        }
    }

    private static (double x1, double x2) SolveQuadraticEquation(double a, double b, double c)
    {
        double D = b * b - 4.0 * a * c;
        if (D > 0.0)
        {
            double rootD = Math.Sqrt(D);
            double x1 = (-b + rootD) / (2.0 * a);
            double x2 = (-b - rootD) / (2.0 * a);
            return (x1, x2);
        }
        else if (D == 0.0) 
        {
            double x = (-b + Math.Sqrt(D)) / (2.0 * a);
            return (x, double.NaN);
        }
        else
        {
            throw new NoRootsException("Вещественных значений не найдено");
            //return (double.NaN, double.NaN);
        }
    }
}