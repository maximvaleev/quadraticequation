﻿using System.Collections;
using System.Text;

namespace QuadraticEquation;

internal class ConsoleHelpers
{
    readonly string _horizonLine = new string('-', 50);
    string[] _paramNames;
    string[] _paramInputStrings;
    double[] _paramValues;
    int _currentSelectedParam = 0;
    int _topOffset;

    public void FormatData (string? message = null,
        Severity severity = Severity.Info,
        IDictionary? data = null,
        int topOffset = -1)
    {
        ConsoleColor fgColor = Console.ForegroundColor;
        ConsoleColor bgColor = Console.BackgroundColor;

        if (topOffset < 0)
            topOffset = Console.CursorTop;

        // Стереть текущее сообщение, если оно было
        FillSectionWithChar(0, topOffset + 1, Console.BufferWidth);
        FillSectionWithChar(0, topOffset, Console.BufferWidth);

        switch (severity)
        {
            case Severity.Info:
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                break;
            case Severity.Warning:
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.DarkYellow;
                break;
            case Severity.Error:
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.DarkRed;
                break;
            default:
                throw new NotImplementedException($"Значение {severity} перечисления" +
                    $" {nameof(Severity)} не учтено в методе!") ;
        }

        Console.WriteLine(_horizonLine);
        if (!string.IsNullOrWhiteSpace(message))
            Console.WriteLine(message);
        Console.WriteLine();
        if (data is not null)
        {
            foreach (DictionaryEntry entry in data)
            {
                Console.WriteLine($"{entry.Key} = {entry.Value}");
            }
        }

        Console.ForegroundColor = fgColor; 
        Console.BackgroundColor = bgColor;
    }

    public double[] GetValuesInteractively(string? message, params string[] paramNames)
    {
        if (paramNames.Length == 0)
            throw new ArgumentException("Число параметров не может быть нулевым.");

        _paramNames = paramNames;
        _paramInputStrings = new string[paramNames.Length];
        _paramValues= new double[paramNames.Length];
        for (int i = 0; i < _paramValues.Length; i++)
            _paramValues[i] = double.NaN;

        Console.WriteLine(message);
        
        _topOffset = Console.CursorTop;

        foreach (string param in _paramNames)
            Console.WriteLine($"  {param}: ");

        FormatData("Заполните коэффициенты. Переключение: стрелки вверх/вниз. Завершить: Enter",
                   Severity.Info,
                   topOffset: _topOffset + _paramNames.Length + 1);

        SelectParam(0);

        StringBuilder sb = new(10);
        ConsoleKeyInfo inputKey;
        bool exitInput = false;
        do
        {
            inputKey = Console.ReadKey(intercept: true);
            switch (inputKey.Key)
            {
                case ConsoleKey.UpArrow:
                    AssignValue(sb.ToString());
                    sb.Clear();
                    SelectNextParam(false);
                    continue;
                case ConsoleKey.DownArrow:
                    AssignValue(sb.ToString());
                    sb.Clear();
                    SelectNextParam();
                    continue;
                case ConsoleKey.Enter:
                    AssignValue(sb.ToString());
                    exitInput = true;
                    break;
                default:
                    if (char.IsDigit(inputKey.KeyChar) || inputKey.KeyChar == ',' || 
                        inputKey.KeyChar == '.' || inputKey.KeyChar == '-')
                    {
                        Console.Write(inputKey.KeyChar);
                        sb.Append(inputKey.KeyChar);
                    }
                    else
                    {
                        FormatData("Для ввода доступны только числовые значения.",
                            Severity.Warning,
                            topOffset: _topOffset + _paramNames.Length + 1);
                        _paramInputStrings[_currentSelectedParam] = sb.ToString();
                        sb.Clear();
                        SelectParam(_currentSelectedParam);
                    }
                    break;
            }
        } 
        while (!exitInput);

        Console.SetCursorPosition(0, _topOffset + _paramNames.Length + 3);

        foreach (double val in _paramValues)
        {
            if (double.IsNaN(val))
            {
                throw new InvalidUserInputException("Введены некорректные или неполные данные.");
            }
        }

        return _paramValues;
    }

    /// <summary>
    /// Попытаться принять введенное значение
    /// </summary>
    private void AssignValue(string valueStr)
    {
        int messageTopPosition = _topOffset + _paramNames.Length + 1;
        _paramValues[_currentSelectedParam] = double.NaN;
        try
        {
            _paramInputStrings[_currentSelectedParam] = valueStr;
            double parsedValue = double.Parse(valueStr.Replace('.', ','));
            if (parsedValue < int.MinValue || parsedValue > int.MaxValue)
            {
                throw new OverflowException();
            }
            _paramValues[_currentSelectedParam] = parsedValue;
            FormatData("Значение принято.", Severity.Info, topOffset: messageTopPosition);
        }
        catch (FormatException)
        {
            FormatData("Введено некорректное значение.", Severity.Error, topOffset: messageTopPosition);
        }
        catch(OverflowException) 
        {
            FormatData($"Введенное значение выходит за диапазон возможных чисел. " +
                $"(Min: {int.MinValue}, max: {int.MaxValue})",
                       Severity.Error,
                       topOffset: messageTopPosition);
        }
        catch(Exception exc)
        {
            FormatData(exc.Message, Severity.Error, topOffset: messageTopPosition);
        }
    }

    /// <summary>
    /// Переключиться на соседний параметр вверх или вниз
    /// </summary>
    private void SelectNextParam(bool moveDown = true)
    {
        if (moveDown)
        {
            if (_currentSelectedParam < _paramNames.Length - 1)
                SelectParam(_currentSelectedParam + 1);
            else
                SelectParam(0);
        }
        else
        {
            if (_currentSelectedParam > 0)
                SelectParam(_currentSelectedParam - 1);
            else
                SelectParam(_paramNames.Length - 1);
        }
    }

    private void SelectParam(int paramIdx)
    {
        // стереть стрелочку на предыдущем выбранном пункте
        FillSectionWithChar(0, _topOffset + _currentSelectedParam, 1);
        // нарисовать стрелочку на новом выбранном пункте
        FillSectionWithChar(0, _topOffset + paramIdx, 1, '>');
        // стереть значение, если оно там было
        if (!string.IsNullOrWhiteSpace(_paramInputStrings[paramIdx]))
        {
            FillSectionWithChar(_paramNames[paramIdx].Length + 4,
                Console.CursorTop,
                _paramInputStrings[paramIdx].Length);
            _paramInputStrings[paramIdx] = string.Empty;
            _paramValues[paramIdx] = double.NaN;
        }
        else
        {
            Console.CursorLeft = _paramNames[paramIdx].Length + 4;
        }
        _currentSelectedParam = paramIdx;
    }

    /// <summary>
    /// Напечатать N указанных символов начиная с указанной координаты. Полезно для стирания.
    /// </summary>
    private void FillSectionWithChar(int leftPos, int topPos, int charsCount, char ch = ' ')
    {
        Console.SetCursorPosition(leftPos, topPos);
        Console.Write(new string(ch, charsCount));
        Console.SetCursorPosition(leftPos, topPos);
    }

    /// <summary>
    /// Попросить пользователя ввести 1 или 0
    /// </summary>
    public static int AskUserForZeroOrOne(string? message = null,
        string answerForZero = "нет",
        string answerForOne = "да")
    {
        if (message != null)
            Console.WriteLine("\n" + message);
        Console.Write($"\t0 - {answerForZero} / 1 - {answerForOne}: ");

        char input;
        do
        {
            input = Console.ReadKey(intercept: true).KeyChar;
        } 
        while (!char.IsDigit(input) || char.GetNumericValue(input) > 1.1);
        Console.WriteLine(input);

        return Convert.ToInt32(char.GetNumericValue(input));
    }
}
